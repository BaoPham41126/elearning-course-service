package com.baopham.elearning.class_;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/class")
public class ClassResource {
	@Autowired
	private ClassService _classService;
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<?> all() {
		return new ResponseEntity<List<Class>>(_classService.findAll(), HttpStatus.OK);
    }
	
	@RequestMapping(value = "/course/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> findByCourseId(@PathVariable(value = "id", required = true) Long id) {
		return new ResponseEntity<List<Class>>(_classService.findByCourse(id), HttpStatus.OK);
    }
	
	@RequestMapping(value = "/one/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> findById(@PathVariable(value = "id", required = true) Long id) {
		return new ResponseEntity<Class>(_classService.findOneById(id), HttpStatus.OK);
    }
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<?> create(@RequestBody @Validated Class _class) throws IOException {
		return new ResponseEntity<Class>(_classService.save(_class), HttpStatus.OK);
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> update(@PathVariable(value = "id", required = true) Long id, @RequestBody @Validated Class _class) {
		return new ResponseEntity<Class>(_classService.update(id, _class), HttpStatus.OK);
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable(value = "id", required = true) Long id) {
		_classService.delete(id);
		return new ResponseEntity<String>("OK deleted", HttpStatus.OK);
    }
	
	@RequestMapping(value = "/{id}/youtube-link", method = RequestMethod.GET)
    public ResponseEntity<?> getYoutubeLinkOfClass(@PathVariable(value = "id", required = true) Long id) throws IOException {
		return new ResponseEntity<List<String>>(_classService.getYoutubeLink(id), HttpStatus.OK);
    }
}