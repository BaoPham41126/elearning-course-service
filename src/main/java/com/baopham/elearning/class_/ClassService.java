package com.baopham.elearning.class_;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.baopham.elearning.course.Course;
import com.baopham.elearning.course.CourseService;
import com.baopham.elearning.utils.Utils;
import com.google.common.collect.Lists;

@Service
public class ClassService {
    public static List<String> scopes = Lists.newArrayList("https://www.googleapis.com/auth/youtube");
    
    @Value(value = "${youtube.link}")
    String youtubeLink;
    
	@Autowired
	ClassRepository _classRepository;
	
	@Autowired
	CourseService courseService;
	
	public Class findOneById(Long id) {
		return _classRepository.findOne(id);
	}
	
	public List<Class> findAll() {
		return _classRepository.findAll();
	}
	
	public List<String> getYoutubeLink(Long id) {
		Class _class = _classRepository.findOne(id);
		if (_class != null) {
			if (_class.getBroadcastIds().size() < 2) {
				return _class.getBroadcastIds();
			} else {
				return _class.getBroadcastIds().subList(_class.getBroadcastIds().size() - 2, _class.getBroadcastIds().size());
			}
		} else {
			return new ArrayList<>();
		}
	}
	
	public List<Class> findByCourse(Long courseId) {
		Course course = courseService.findOneById(courseId);
		if (course != null)
			return _classRepository.findByCourse(course);
		else
			return new ArrayList<Class>();
	}
	
	public Class save(Class class_) throws IOException {
		return _classRepository.save(class_);
	}
	
	public Class update(Long id, Class _class) {
		Class oldClass = this.findOneById(id);
		if (oldClass != null) {
			BeanUtils.copyProperties(_class, oldClass, Utils.getNullPropertyNames(_class));
		}
		return _classRepository.save(oldClass);
	}
	
	public void delete(Long id) {
		Class _class = this.findOneById(id);
		if (_class != null) {
			_classRepository.delete(_class);
		}
	}
}
