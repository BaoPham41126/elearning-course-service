package com.baopham.elearning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.baopham.elearning.class_.ClassService;
import com.baopham.elearning.youtube.Auth;

@SpringBootApplication
public class App {

    public static void main(String[] args) throws Exception{
        SpringApplication.run(App.class, args);
		Auth.authorize(ClassService.scopes, "all");
    }
}
