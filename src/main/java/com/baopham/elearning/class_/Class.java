package com.baopham.elearning.class_;

import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.baopham.elearning.course.Course;

@Entity
@Table(name = "class_entity")
public class Class {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	Long id;
	
	String name;
	
	@ElementCollection
	@CollectionTable(name="youtube_broadcast", joinColumns=@JoinColumn(name="class_id"))
	List<String> broadcastIds;
	
	@ManyToOne
	@JoinColumn(name="course_id", nullable=false)
	Course course;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public List<String> getBroadcastIds() {
		return broadcastIds;
	}

	public void setBroadcastIds(List<String> broadcastIds) {
		this.broadcastIds = broadcastIds;
	}
	
	
}
