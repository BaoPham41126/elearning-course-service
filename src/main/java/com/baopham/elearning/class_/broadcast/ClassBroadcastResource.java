package com.baopham.elearning.class_.broadcast;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/class-broadcast")
public class ClassBroadcastResource {
	@Autowired
	private ClassBroadcastService _classBroadcastService;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public ResponseEntity<?> createNewBroadcast(@PathVariable(value = "id", required = true) Long id) throws IOException {
		return new ResponseEntity<String>(_classBroadcastService.createBroadcastForClass(id), HttpStatus.OK);
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> closeAllBroadcast(@PathVariable(value = "id", required = true) Long id) throws IOException {
		_classBroadcastService.closeAllBroadcast(id);
		return new ResponseEntity<String>("OK closed", HttpStatus.OK);
    }
}