package com.baopham.elearning.course;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baopham.elearning.utils.Utils;

@Service
public class CourseService {
	@Autowired
	CourseRepository courseRepository;
	
	public Course findOneById(Long id) {
		return courseRepository.findOne(id);
	}
	
	public List<Course> findAll() {
		return courseRepository.findAll();
	}
	
	public Course save(Course course) {
		return courseRepository.save(course);
	}
	
	public Course update(Long id, Course course) {
		Course oldCourse = this.findOneById(id);
		if (oldCourse != null) {
			BeanUtils.copyProperties(course, oldCourse, Utils.getNullPropertyNames(course));
		}
		return courseRepository.save(oldCourse);
	}
	
	public void delete(Long id) {
		Course course = this.findOneById(id);
		if (course != null) {
			courseRepository.delete(course);
		}
	}
}
