package com.baopham.elearning.class_.broadcast;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.baopham.elearning.class_.Class;
import com.baopham.elearning.class_.ClassService;
import com.baopham.elearning.course.CourseService;
import com.baopham.elearning.youtube.Auth;
import com.baopham.elearning.youtube.BroadCastManagementThread;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.util.DateTime;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.YouTube.LiveBroadcasts.Transition;
import com.google.api.services.youtube.model.CdnSettings;
import com.google.api.services.youtube.model.LiveBroadcast;
import com.google.api.services.youtube.model.LiveBroadcastContentDetails;
import com.google.api.services.youtube.model.LiveBroadcastSnippet;
import com.google.api.services.youtube.model.LiveBroadcastStatus;
import com.google.api.services.youtube.model.LiveStream;
import com.google.api.services.youtube.model.LiveStreamSnippet;
import com.google.api.services.youtube.model.MonitorStreamInfo;
import com.google.common.collect.Lists;

@Service
public class ClassBroadcastService {
    public static List<String> scopes = Lists.newArrayList("https://www.googleapis.com/auth/youtube");
    
    @Value(value = "${youtube.link}")
    String youtubeLink;
    
	@Autowired
	ClassService classService;
	
	@Autowired
	CourseService courseService;
	
	String createBroadcastForClass(Long id) throws IOException{
		Class class_ = classService.findOneById(id);
		if (class_ != null){
			return this.createBroadcastForClass(class_);
		} else {
			return null;
		}
	}
	
	LiveStream createNewStream(Class class_) throws IOException{
		Credential credential =  Auth.authorize(scopes, "all");
	    YouTube youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT, Auth.JSON_FACTORY, credential)
                .setApplicationName("VideoUpload").build();;
	    
		String title = class_.getName();
        LiveStreamSnippet streamSnippet = new LiveStreamSnippet();
        streamSnippet.setTitle(title);
        CdnSettings cdnSettings = new CdnSettings();
        cdnSettings.setFormat("1080p");
        cdnSettings.setIngestionType("rtmp");

        LiveStream stream = new LiveStream();
        stream.setKind("youtube#liveStream");
        stream.setSnippet(streamSnippet);
        stream.setCdn(cdnSettings);

        // Construct and execute the API request to insert the stream.
        YouTube.LiveStreams.Insert liveStreamInsert =
                youtube.liveStreams().insert("status,snippet,cdn", stream);
        LiveStream returnedStream = liveStreamInsert.execute();
        return returnedStream;
	}
	
	String createBroadcastForClass(Class class_) throws IOException{
		Credential credential = Auth.authorize(scopes, "all");
		YouTube youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT, Auth.JSON_FACTORY, credential)
                .setApplicationName("VideoUpload").build();
		
		Date currentDate = new Date();

        String title = class_.getName() + " " + currentDate;
        LiveBroadcastSnippet broadcastSnippet = new LiveBroadcastSnippet();
        broadcastSnippet.setTitle(title);
        broadcastSnippet.setScheduledStartTime(new DateTime(currentDate));

        LiveBroadcastStatus status = new LiveBroadcastStatus();
        status.setPrivacyStatus("public");
        
        LiveBroadcastContentDetails contentDetails = new LiveBroadcastContentDetails();
        MonitorStreamInfo monitorStreamInfo = new MonitorStreamInfo();
        monitorStreamInfo.setEnableMonitorStream(false);
        contentDetails.setMonitorStream(monitorStreamInfo);

        LiveBroadcast broadcast = new LiveBroadcast();
        broadcast.setKind("youtube#liveBroadcast");
        broadcast.setSnippet(broadcastSnippet);
        broadcast.setStatus(status);
        broadcast.setContentDetails(contentDetails);

        YouTube.LiveBroadcasts.Insert liveBroadcastInsert = youtube.liveBroadcasts().insert("snippet,status,contentDetails", broadcast);
        LiveBroadcast returnedBroadcast = liveBroadcastInsert.execute();
        
        LiveStream stream = this.createNewStream(class_);

        YouTube.LiveBroadcasts.Bind liveBroadcastBind =
                youtube.liveBroadcasts().bind(returnedBroadcast.getId(), "id,status,contentDetails");
        liveBroadcastBind.setStreamId(stream.getId());
        returnedBroadcast = liveBroadcastBind.execute();
        class_.getBroadcastIds().add(returnedBroadcast.getId());
        classService.save(class_);
        
        BroadCastManagementThread broadCastManagementThread = new BroadCastManagementThread(returnedBroadcast, stream, youtube);
        Thread thread = new Thread(broadCastManagementThread);
        thread.start();
        
        return stream.getCdn().getIngestionInfo().getIngestionAddress() + "/" + stream.getCdn().getIngestionInfo().getStreamName();
	}
	
	void closeAllBroadcast(Long classId) throws IOException {
		Class class_ = classService.findOneById(classId);
		if (class_ == null) {
			return;
		}
		Credential credential = Auth.authorize(scopes, "all");
		YouTube youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT, Auth.JSON_FACTORY, credential)
                .setApplicationName("VideoUpload").build();
		for (String broadcastId : class_.getBroadcastIds()) {
			try {
				Transition transitionRequest = youtube.liveBroadcasts().transition("complete", broadcastId, "status");
				transitionRequest.execute();
			} catch(Exception e) {
				
			}
		}
		class_.getBroadcastIds().clear();
		classService.save(class_);
	}
}