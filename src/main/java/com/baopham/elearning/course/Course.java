package com.baopham.elearning.course;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.baopham.elearning.class_.Class;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "course_entity")
public class Course {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	Long id;
	
	@NotNull
	@NotBlank
	String name;
	
	@OneToMany
	@JoinColumn(name="course_id")
	@JsonIgnore
	List<Class> classes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Class> getClasses() {
		return classes;
	}

	public void setClasses(List<Class> classes) {
		this.classes = classes;
	}
} 
