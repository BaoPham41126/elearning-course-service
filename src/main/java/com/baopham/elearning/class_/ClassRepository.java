package com.baopham.elearning.class_;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.baopham.elearning.course.Course;

@Repository
public interface ClassRepository extends JpaRepository<Class, Long> {
	public List<Class> findByCourse(Course course);
}
