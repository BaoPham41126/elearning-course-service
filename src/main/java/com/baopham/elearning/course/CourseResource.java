package com.baopham.elearning.course;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/course")
public class CourseResource {
	@Autowired
	private CourseService courseService;
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<?> all() {
		return new ResponseEntity<List<Course>>(courseService.findAll(), HttpStatus.OK);
    }
	
	@RequestMapping(value = "/one/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> findById(@PathVariable(value = "id", required = true) Long id) {
		return new ResponseEntity<Course>(courseService.findOneById(id), HttpStatus.OK);
    }
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<?> create(@RequestBody @Validated Course course) {
		return new ResponseEntity<Course>(courseService.save(course), HttpStatus.OK);
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> update(@PathVariable(value = "id", required = true) Long id, @RequestBody @Validated Course course) {
		return new ResponseEntity<Course>(courseService.update(id, course), HttpStatus.OK);
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable(value = "id", required = true) Long id) {
		courseService.delete(id);
		return new ResponseEntity<String>("OK deleted", HttpStatus.OK);
    }
	
}